const AuthController = require('./auth_controller');
const UserController = require('./user_controller');
const ArticleController = require('./article_controller');
const CommentController = require('./comment_controller');

/**
 * Controller singleton initialization
 */
exports.authController = new AuthController();
exports.userController = new UserController();
exports.articleController = new ArticleController();
exports.commentController = new CommentController();
