const {
  AuthService,
  UserService,
} = require('../service');

const ResponseBuilder = require('../utils/response_builder');
const BaseController = require('./base_controller');

class AuthController extends BaseController {
  /**
   * Auth controller class
   */
  constructor() {
    super(new AuthService());
    this.userService = new UserService();
  }

  validatePassword(password) {
    /**
     * move validation of password out of model
     * since we are using hash+salt to store those information.
     */
    let errorData = [];
    if (!password) return ['password must be provided.'];
    if (password.length < 8)
      errorData.push('password must not be shorter than 8 character.');
    return errorData;
  }

  async register(req, res) {
    const { email, password } = req.body;
    let responseBuilder = new ResponseBuilder();
    let errorData = this.validatePassword(password);

    if (errorData.length > 0) {
      this.sendInvalidPayloadResponse(
        res,
        responseBuilder
          .setErrors(errorData)
          .setMessage(errorData[errorData.length - 1])
          .setSuccess(false)
          .build()
      );
      return;
    }

    try {
      const result = await this.userService.register(
        email,
        password,
      );
      this.sendCreatedResponse(
        res,
        responseBuilder
          .setData(result)
          .setMessage('user registered successfully')
          .build()
      );
      return;
    } catch (error) {
      if (error.name == 'SequelizeValidationError') {
        const errorData = error.errors.map(item => {
          return item.message;
        });
        this.sendInvalidPayloadResponse(
          res,
          responseBuilder
            .setErrors(errorData)
            .setMessage(errorData[errorData.length - 1])
            .setSuccess(false)
            .build()
        );
        return;
      } else if (error.name == 'SequelizeUniqueConstraintError') {
        const errorData = error.errors.map(item => {
          return item.message;
        });
        this.sendResourceAlreadyExistResponse(
          res,
          responseBuilder
            .setErrors(errorData)
            .setMessage(errorData[errorData.length - 1])
            .setSuccess(false)
            .build()
        );
        return;
      }
      this.sendResourceAlreadyExistResponse(
        res,
        responseBuilder
          .setSuccess(false)
          .setMessage(error.toString())
          .build()
      );
    }
  }

  async regenerateToken(req, res) {
    const { refresh_token: refreshToken } = req.body;
    let responseBuilder = new ResponseBuilder();
    if (!refreshToken) {
      this.sendInvalidPayloadResponse(
        res,
        responseBuilder
          .setMessage('Invalid payload provided')
          .setSuccess(false)
          .build()
      );
      return;
    }
  }

  async generateToken(req, res) {
    const { email, password } = req.body;
    let responseBuilder = new ResponseBuilder();
    if (!email || !password) {
      this.sendInvalidPayloadResponse(
        res,
        responseBuilder
          .setMessage('Invalid payload provided')
          .setSuccess(false)
          .build()
      );
      return;
    }
    try {
      const user = await this.userService.findOne({ email });

      if (!user) {
        this.sendNotFoundResponse(
          res,
          responseBuilder
            .setSuccess(false)
            .setMessage('has not been registered yet.')
            .build()
        );
        return;
      }
      if (!this.userService.checkLogin(user, password)) {
        this.sendInvalidPayloadResponse(
          res,
          responseBuilder
            .setSuccess(false)
            .setMessage('invalid email / password.')
            .build()
        );
        return;
      }
      const token = await this.service.generateJWToken({
        email: user.email,
        id: user.id
      });
      /**
       * TODO: add refresh token
       * CAUSE: omit refresh token for simplicity of the program now
       */
      this.sendSuccessResponse(
        res,
        responseBuilder
          .setData({
            access_token: token,
            user
          })
          .setMessage('token generated successfully')
          .build()
      );
      return;
    } catch (error) {
      this.sendInvalidPayloadResponse(
        res,
        responseBuilder
          .setSuccess(false)
          .setMessage(error.toString())
          .build()
      );
    }
  }
}

module.exports = AuthController
