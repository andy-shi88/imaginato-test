const { ArticleService } = require('../service');
const ResponseBuilder = require('../utils/response_builder');
const BaseController = require('./base_controller');
const models = require('../models');

class ArticleController extends BaseController {
  /**
   * Article controller class
   */
  constructor() {
    super(new ArticleService());
  }

  async create(req, res) {
    let responseBuilder = new ResponseBuilder();
    const {
      title,
      nickname,
      content,
    } = req.body;
    try {
      if (
        !title ||
        !nickname ||
        !content
      ) {
        this.sendInvalidPayloadResponse(
          res,
          responseBuilder
            .setSuccess(false)
            .setMessage('invalid payload for creating new article')
            .build()
        );
      }
      const userId = res.locals.user.id;
      const payload = {
        title,
        nickname,
        content,
        userId,
      };
      const article = await this.service.create(payload);
      this.sendCreatedResponse(
        res,
        responseBuilder
          .setData(article)
          .setMessage('article created')
          .build()
      );
    } catch (error) {
      console.log(error, 'error')
      this.sendInternalServerErrorResponse(
        res,
        responseBuilder
          .setSuccess(false)
          .setMessage('server fault, please try again later')
          .build()
      );
    }
  }

  async get(req, res) {
    const { page, limit, fields, order } = req.query;
    let responseBuilder = new ResponseBuilder();
    try {
      const result = await this.service.paginate(
        req,
        page,
        limit,
        order,
        fields
      );

      this.sendSuccessResponse(
        res,
        responseBuilder
          .setData(result.data)
          .setLinks(result.links)
          .setTotal(result.total)
          .setCount(result.count)
          .setMessage('articles fetched successfully')
          .build()
      );
      return;
    } catch (error) {
      this.sendBadRequestResponse(
        res,
        responseBuilder
          .setSuccess(false)
          .setMessage(error.toString())
          .build()
      );
    }
  }

  async find(req, res) {
    const { id } = req.params;
    let responseBuilder = new ResponseBuilder();
    try {
      const result = await this.service.findOne({ id }, [
        {
          model: models.Comment,
          as: 'comments'
        }
      ]);
      if (!result) {
        this.sendNotFoundResponse(
          res,
          responseBuilder
            .setSuccess(false)
            .setMessage('article not found')
            .build()
        );
        return;
      }
      this.sendSuccessResponse(
        res,
        responseBuilder
          .setData(result)
          .setMessage('article fetched successfully')
          .build()
      );
      return;
    } catch (error) {
      this.sendBadRequestResponse(
        res,
        responseBuilder
          .setSuccess(false)
          .setMessage(error.toString())
          .build()
      );
    }
  }
}

module.exports = ArticleController