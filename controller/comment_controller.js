const { CommentService } = require('../service');
const ResponseBuilder = require('../utils/response_builder');
const BaseController = require('./base_controller');
const models = require('../models');

class CommentController extends BaseController {
  /**
   * Article controller class
   */
  constructor() {
    super(new CommentService());
  }

  async create(req, res) {
    const { id: articleId } = req.params;
    let responseBuilder = new ResponseBuilder();
    const {
      nickname,
      content,
    } = req.body;
    try {
      if (
        !nickname ||
        !content ||
        !articleId
      ) {
        this.sendInvalidPayloadResponse(
          res,
          responseBuilder
            .setSuccess(false)
            .setMessage('invalid payload for creating new comment')
            .build()
        );
      }
      const userId = res.locals.user.id;
      const payload = {
        nickname,
        content,
        userId,
        articleId,
      };
      const comment = await this.service.create(payload);
      this.sendCreatedResponse(
        res,
        responseBuilder
          .setData(comment)
          .setMessage('comment created')
          .build()
      );
    } catch (error) {
      console.log(error, 'error')
      this.sendInternalServerErrorResponse(
        res,
        responseBuilder
          .setSuccess(false)
          .setMessage('server fault, please try again later')
          .build()
      );
    }
  }

  async createChild(req, res) {
    const { id: commentId } = req.params;
    let responseBuilder = new ResponseBuilder();
    const {
      nickname,
      content,
    } = req.body;
    try {
      if (
        !nickname ||
        !content ||
        !commentId
      ) {
        this.sendInvalidPayloadResponse(
          res,
          responseBuilder
            .setSuccess(false)
            .setMessage('invalid payload for creating new comment')
            .build()
        );
      }
      const userId = res.locals.user.id;
      const payload = {
        nickname,
        content,
        userId,
        commentId,
      };
      const comment = await this.service.create(payload);
      this.sendCreatedResponse(
        res,
        responseBuilder
          .setData(comment)
          .setMessage('comment created')
          .build()
      );
    } catch (error) {
      console.log(error, 'error')
      this.sendInternalServerErrorResponse(
        res,
        responseBuilder
          .setSuccess(false)
          .setMessage('server fault, please try again later')
          .build()
      );
    }
  }

  async get(req, res) {
    const { page, limit, fields, order } = req.query;
    let responseBuilder = new ResponseBuilder();
    try {
      const result = await this.service.paginate(
        req,
        page,
        limit,
        order,
        fields
      );

      this.sendSuccessResponse(
        res,
        responseBuilder
          .setData(result.data)
          .setLinks(result.links)
          .setTotal(result.total)
          .setCount(result.count)
          .setMessage('comments fetched successfully')
          .build()
      );
      return;
    } catch (error) {
      this.sendBadRequestResponse(
        res,
        responseBuilder
          .setSuccess(false)
          .setMessage(error.toString())
          .build()
      );
    }
  }

  async find(req, res) {
    const { id } = req.params;
    let responseBuilder = new ResponseBuilder();
    try {
      const result = await this.service.findOne({ id }, [
        {
          model: models.Comment,
          as: 'child_comments'
        }
      ]);
      if (!result) {
        this.sendNotFoundResponse(
          res,
          responseBuilder
            .setSuccess(false)
            .setMessage('comment not found')
            .build()
        );
        return;
      }
      this.sendSuccessResponse(
        res,
        responseBuilder
          .setData(result)
          .setMessage('comment fetched successfully')
          .build()
      );
      return;
    } catch (error) {
      this.sendBadRequestResponse(
        res,
        responseBuilder
          .setSuccess(false)
          .setMessage(error.toString())
          .build()
      );
    }
  }

}

module.exports = CommentController