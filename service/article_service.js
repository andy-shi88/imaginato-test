const BaseService = require('./base_service');
const models = require('../models');

class ArticleService extends BaseService {
  /**
   * Article specific service class
   */

  constructor() {
    super(models.Article);
  }
}

module.exports = ArticleService;
