const bcrypt = require('bcrypt');
const csprng = require('csprng');
const BaseService = require('./base_service');
const models = require('../models');

class UserService extends BaseService {
  /**
   * User specific service class
   */
  constructor() {
    super(models.User);
  }

  async register(email, password) {
    const salt = csprng(162, 36);
    const payload = {
      password: bcrypt.hashSync(password + salt, 10),
      salt,
      email,
    };
    return this.create(payload);
  }

  checkLogin(user, password) {
    return bcrypt.compareSync(password + user.salt, user.password);
  }
}

module.exports = UserService
