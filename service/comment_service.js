const BaseService = require('./base_service');
const models = require('../models');

class CommentService extends BaseService {
  /**
   * Comment specific service class
   */

  constructor() {
    super(models.Comment);
  }
}

module.exports = CommentService;
