'use strict';

module.exports = (sequelize, DataTypes) => {
  const Comment = sequelize.define(
    'Comment',
    {
      content: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: { msg: 'Please input comment content' }
        }
      },
      nickname: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: { msg: 'Please input comment nickname' }
        }
      },
      userId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'users',
          key: 'id',
        },
        field: 'user_id',
      },
      articleId: {
        type: DataTypes.INTEGER,
        allowNull: true,
        references: {
          model: 'articles',
          key: 'id',
        },
        field: 'article_id',
      },
      commentId: {
        type: DataTypes.INTEGER,
        allowNull: true,
        references: {
          model: 'comments',
          key: 'id',
        },
        field: 'comment_id',
      },
    },
    {
      createdAt: 'created_at',
      updatedAt: 'updated_at',
      timestamps: true,
      underscored: true,
      tableName: 'comments'
    }
  );
  Comment.associate = function(models) {
    Comment.belongsTo(models.Article, { foreignKey: 'articleId', as: 'comments' })
    Comment.hasMany(models.Comment, { foreignKey: 'commentId', as: 'child_comments' })
  };
  return Comment;
};
