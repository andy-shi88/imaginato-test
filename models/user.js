'use strict';

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    'User',
    {
      email: {
        type: DataTypes.STRING,
        unique: {
          args: true,
          msg: 'email already in use!'
        },
        allowNull: false,
        validate: {
          isEmail: {
            msg: 'Please input an email address'
          },
          notEmpty: { msg: 'Please input email address' }
        }
      },
      password: {
        type: DataTypes.STRING,
        min: {
          args: 8,
          msg: 'password must be at least 8 characters.'
        },
        allowNull: false
      },
      salt: DataTypes.STRING(64),
      deletedAt: {
        type: DataTypes.DATE,
        defaultValue: null,
        allowNull: true,
        field: 'deleted_at'
      }
    },
    {
      createdAt: 'created_at',
      updatedAt: 'updated_at',
      timestamps: true,
      underscored: true,
      tableName: 'users'
    }
  );
  User.associate = function(models) {
  };
  User.prototype.toJSON = function() {
    let values = Object.assign({}, this.get());
    delete values.salt;
    delete values.password;
    return values;
  };
  return User;
};
