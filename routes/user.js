const express = require('express');
const { userController } = require('../controller');

const router = express.Router();

router.get('/', (req, res) => {
  userController.get(req, res);
});

router.get('/:id', (req, res) => {
  userController.find(req, res);
});

module.exports = router;
