const express = require('express');
const { articleController, commentController } = require('../controller');

const AuthUser = require('../utils/auth_user');

const router = express.Router();

router.use(AuthUser);

router.get('/', (req, res) => {
  articleController.get(req, res);
});

router.post('/', (req, res) => {
  articleController.create(req, res);
});

router.get('/:id', (req, res) => {
  articleController.find(req, res);
});

router.post('/:id/comments', (req, res) => {
  commentController.create(req, res);
});

router.get('/:id/comments', (req, res) => {
  articleController.get(req, res);
});

module.exports = router;
