const express = require('express');
const { commentController } = require('../controller');

const AuthUser = require('../utils/auth_user');

const router = express.Router();

router.use(AuthUser);

router.get('/:id', (req, res) => {
  commentController.find(req, res);
});

router.post('/:id/comment', (req, res) => {
  commentController.createChild(req, res);
});

module.exports = router;
