### Node - Imaginato test.

#### specs:
- ##### node version: 10.15
- ##### database: mysql
- ##### schema + orm : sequelize


#### Run

- create database called `article_interview`
- prepare project dependencies by running `npm install` 
- configure db setting at `config/config.json`
- run db schema migration `./node_modules/.bin/sequelize db:migrate`

- run the development app server by running `node index.js` from project root


#### flow
Every endpoint is stored and commited as postman collection.
- register user by providing email and password.
- login user and get the authorization header (JWT token)

for every next request, provide `Authorization` header by prefix of `bearer your-access-token-here`.

- post article `api/v1/articles - POST`
- comment on article `api/v1/articles/:articleId/comment - POST`
- get all article (without comment) `api/v1/articles - GET`
- get single article (with comments) `api/v1/articles/:articleId - GET`
- get single comments (with comments of comment) `api/v1/comments/:commentId - GET`
- post comment on comment `api/v1/comments/:commentId - POST`


payload and endpoint is provided at the postman collection, please refer to those.


### notes:
- jwt expiration is configurable through .env var.
- jwt secret is configurable through .env var.
- logs path is also configured through .env 