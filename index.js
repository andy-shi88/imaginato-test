const express = require('express');
const bodyParser = require('body-parser');
const logger = require('morgan');
const fs = require('fs');
// API handlers
const auth = require('./routes/auth');
const user = require('./routes/user');
const article = require('./routes/article');
const comment = require('./routes/comment');

require('dotenv').config();

const app = express();

// set logger see: https://www.npmjs.com/package/morgan for log formatting.
// console log
app.use(logger('dev'));
// file log
app.use(
  logger('common', {
    stream: fs.createWriteStream(process.env.LOGGER_FILE_PATH, { flags: 'w' })
  })
);
// set body parser
app.use(bodyParser.json({ limit: '1mb' }));

// port setup
app.set('port', process.env.PORT || 3000);

// Set API endpoints handlers
app.use('/api/v1/auth', auth);
app.use('/api/v1/users', user);
app.use('/api/v1/articles', article);
app.use('/api/v1/comments', comment);
// rate limit per endpoint: this could be use if we want different rate limit rule on certain endpoint.
// app.use('/api/v1/auth', customLimiter);

app.listen(app.get('port'), () => {});

module.exports = app;
